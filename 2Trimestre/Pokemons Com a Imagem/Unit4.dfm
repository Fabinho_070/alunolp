object DataModule4: TDataModule4
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 147
  Width = 369
  object FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink
    VendorLib = 
      'C:\Users\Informatica 03\Documents\EasyPHP-DevServer-14.1VC9\bina' +
      'ries\mysql\lib\libmysql.dll'
    Left = 32
    Top = 24
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=pokedex'
      'User_Name=root'
      'DriverID=MySQL')
    Left = 120
    Top = 24
  end
  object FDQueryPokemon: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'Select * from  pokemon')
    Left = 208
    Top = 24
  end
  object DataSourcePokemon: TDataSource
    DataSet = FDQueryPokemon
    Left = 296
    Top = 24
  end
  object FDQueryTreinador: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'Select * from treinador')
    Left = 96
    Top = 88
  end
  object DataSourceTreinador: TDataSource
    DataSet = FDQueryTreinador
    Left = 176
    Top = 88
  end
end
