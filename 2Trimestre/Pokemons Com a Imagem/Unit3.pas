unit Unit3;
interface
uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.DBCtrls, Vcl.StdCtrls,Vcl.Imaging.pngimage, Vcl.ExtCtrls, Unit4, Unit5;

type
  TForm3 = class(TForm)
    DBLookupListBoxPokemon: TDBLookupListBox;
    ButtonInserir: TButton;
    ButtonDeletar: TButton;
    ButtonEditar: TButton;
    DBTextId: TDBText;
    DBTextNome: TDBText;
    DBTextTreinador: TDBText;
    DBTextNivel: TDBText;
    Image1: TImage;
    procedure ButtonDeletarClick(Sender: TObject);
    procedure ButtonInserirClick(Sender: TObject);
    procedure ButtonEditarClick(Sender: TObject);
    procedure DBLookupListBoxPokemonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}

procedure TForm3.ButtonDeletarClick(Sender: TObject);
begin

DataModule4.FDQueryPokemon.Delete();

end;

procedure TForm3.ButtonEditarClick(Sender: TObject);
begin

Form5.Show();

end;

procedure TForm3.ButtonInserirClick(Sender: TObject);
begin

DataModule4.FDQueryPokemon.Append();
Form5.Show();

end;

procedure TForm3.DBLookupListBoxPokemonClick(Sender: TObject);
var url:string;
begin

url:= DataModule4.FDQueryPokemon.FieldByName('nome').AsString;
Image1.Picture.LoadFromFile('C:\Users\Informatica 03\Documents\' + LowerCase(url) + '.png');

end;

end.
