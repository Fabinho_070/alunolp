unit Unit5;
interface
uses
Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Unit4, Vcl.StdCtrls, Vcl.DBCtrls,
Vcl.Mask;

type
  TForm5 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdit3: TDBEdit;
    DBLookupComboBox1: TDBLookupComboBox;
    ButtonCancelar: TButton;
    ButtonSalvar: TButton;
    procedure ButtonCancelarClick(Sender: TObject);
    procedure ButtonSalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form5: TForm5;

implementation

{$R *.dfm}

procedure TForm5.ButtonCancelarClick(Sender: TObject);
begin
  Close();
end;

procedure TForm5.ButtonSalvarClick(Sender: TObject);
begin
  DataModule4.FDQueryPokemon.Post();
  Close();
end;

end.
