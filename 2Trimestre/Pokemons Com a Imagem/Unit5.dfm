object Form5: TForm5
  Left = 0
  Top = 0
  Caption = 'Salvar Pokemons'
  ClientHeight = 268
  ClientWidth = 283
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 48
    Width = 89
    Height = 13
    Caption = 'Nome Do Pokemon'
  end
  object Label2: TLabel
    Left = 8
    Top = 96
    Width = 108
    Height = 13
    Caption = 'Treinador Do Pokemon'
  end
  object Label3: TLabel
    Left = 12
    Top = 139
    Width = 85
    Height = 13
    Caption = 'Nivel Do Pokemon'
  end
  object DBEdit1: TDBEdit
    Left = 128
    Top = 45
    Width = 121
    Height = 21
    DataField = 'nome'
    DataSource = DataModule4.DataSourcePokemon
    TabOrder = 0
  end
  object DBEdit3: TDBEdit
    Left = 128
    Top = 136
    Width = 121
    Height = 21
    DataField = 'nivel'
    DataSource = DataModule4.DataSourcePokemon
    TabOrder = 1
  end
  object DBLookupComboBox1: TDBLookupComboBox
    Left = 128
    Top = 88
    Width = 145
    Height = 21
    DataField = 'id_treinador'
    DataSource = DataModule4.DataSourcePokemon
    KeyField = 'id'
    ListField = 'nome'
    ListSource = DataModule4.DataSourceTreinador
    TabOrder = 2
  end
  object ButtonCancelar: TButton
    Left = 96
    Top = 177
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 3
    OnClick = ButtonCancelarClick
  end
  object ButtonSalvar: TButton
    Left = 96
    Top = 224
    Width = 75
    Height = 25
    Caption = 'Salvar'
    TabOrder = 4
    OnClick = ButtonSalvarClick
  end
end
