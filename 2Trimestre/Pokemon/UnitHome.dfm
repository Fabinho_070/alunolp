object Home: THome
  Left = 0
  Top = 0
  Caption = 'Home'
  ClientHeight = 414
  ClientWidth = 425
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object LabelPokemon: TLabel
    Left = 202
    Top = 21
    Width = 47
    Height = 13
    Caption = 'Pok'#233'mon:'
  end
  object EditPokemonName: TEdit
    Left = 112
    Top = 40
    Width = 225
    Height = 21
    TabOrder = 0
    TextHint = 'Insira o nome do seu pok'#233'mon'
  end
  object ButtonAdd: TButton
    Left = 272
    Top = 67
    Width = 121
    Height = 25
    Caption = 'Novo'
    TabOrder = 1
    OnClick = ButtonAddClick
  end
  object ButtonDelete: TButton
    Left = 272
    Top = 218
    Width = 121
    Height = 25
    Caption = 'Deletar'
    TabOrder = 2
    OnClick = ButtonDeleteClick
  end
  object ButtonDeleteAll: TButton
    Left = 272
    Top = 308
    Width = 121
    Height = 25
    Caption = 'Deletar Todos'
    TabOrder = 3
    OnClick = ButtonDeleteAllClick
  end
  object ListBoxPokemon: TListBox
    Left = 24
    Top = 67
    Width = 225
    Height = 266
    ItemHeight = 13
    TabOrder = 4
  end
  object editar: TButton
    Left = 272
    Top = 136
    Width = 121
    Height = 25
    Caption = 'Editar'
    TabOrder = 5
    OnClick = editarClick
  end
end
