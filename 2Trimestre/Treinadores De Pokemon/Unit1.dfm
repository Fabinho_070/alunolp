object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'PokeCenter'
  ClientHeight = 527
  ClientWidth = 710
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 352
    Top = 23
    Width = 61
    Height = 13
    Caption = 'Identificador'
  end
  object Label2: TLabel
    Left = 352
    Top = 79
    Width = 46
    Height = 13
    Caption = 'Treinador'
  end
  object Label3: TLabel
    Left = 352
    Top = 165
    Width = 27
    Height = 13
    Caption = 'Nome'
  end
  object Label4: TLabel
    Left = 352
    Top = 229
    Width = 23
    Height = 13
    Caption = 'Nivel'
  end
  object Button1: TButton
    Left = 328
    Top = 271
    Width = 300
    Height = 25
    Caption = 'Inserir Pokemon'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 328
    Top = 302
    Width = 300
    Height = 25
    Caption = 'Editar Pokemon'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 328
    Top = 333
    Width = 300
    Height = 25
    Caption = 'Deletar Pokemon'
    TabOrder = 2
    OnClick = Button3Click
  end
  object Panel1: TPanel
    Left = 344
    Top = 8
    Width = 284
    Height = 257
    TabOrder = 3
  end
  object DBEdit1: TDBEdit
    Left = 472
    Top = 20
    Width = 121
    Height = 21
    DataField = 'id'
    DataSource = DataModule2.DataSourcePokemon
    TabOrder = 4
  end
  object DBEdit2: TDBEdit
    Left = 472
    Top = 76
    Width = 121
    Height = 21
    DataField = 'nome'
    DataSource = DataModule2.DataSourceTreinador
    TabOrder = 5
  end
  object DBEdit3: TDBEdit
    Left = 472
    Top = 162
    Width = 121
    Height = 21
    DataField = 'nome'
    DataSource = DataModule2.DataSourcePokemon
    TabOrder = 6
  end
  object DBEdit4: TDBEdit
    Left = 472
    Top = 226
    Width = 121
    Height = 21
    DataField = 'nivel'
    DataSource = DataModule2.DataSourcePokemon
    TabOrder = 7
  end
  object DBLookupListBox1: TDBLookupListBox
    Left = 8
    Top = 8
    Width = 289
    Height = 251
    KeyField = 'nome'
    ListField = 'nome'
    ListSource = DataModule2.DataSourcePokemon
    TabOrder = 8
  end
end
