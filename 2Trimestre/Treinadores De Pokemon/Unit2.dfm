object DataModule2: TDataModule2
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 419
  Width = 617
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=fabinho_pkm'
      'User_Name=root'
      'DriverID=mySQL')
    Connected = True
    Left = 152
    Top = 56
  end
  object DataSourcePokemon: TDataSource
    DataSet = FDQueryPokemon
    Left = 296
    Top = 56
  end
  object FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink
    VendorLib = 'C:\EasyPHP-DevServer-14.1VC9\binaries\mysql\lib\libmysql.dll'
    Left = 56
    Top = 56
  end
  object FDQueryPokemon: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'SELECT * FROM pokemon')
    Left = 224
    Top = 56
  end
  object FDQuerytreinador: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'SELECT * FROM treinador')
    Left = 224
    Top = 128
  end
  object DataSourceTreinador: TDataSource
    DataSet = FDQuerytreinador
    Left = 304
    Top = 128
  end
end
