unit Unit01;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Imaging.jpeg,
  Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    LBJornal: TListBox;
    btnEnviar: TButton;
    btnDeletar: TButton;
    btnAtualizar: TButton;
    btnSalvar: TButton;
    btnCarregar: TButton;
    EEdicao: TEdit;
    ETema: TEdit;
    EAutor: TEdit;
    Image1: TImage;
    LJornal: TLabel;
    procedure btnEnviarClick(Sender: TObject);
    procedure btnAtualizarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnDeletarClick(Sender: TObject);
    procedure btnCarregarClick(Sender: TObject);
    procedure LBJornalClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TJornal = class(TObject)
     tema, edicao, autor: string;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btnAtualizarClick(Sender: TObject);
var jornal : TJornal;
var tema, edicao, autor: string;
begin
  edicao:=EEdicao.Text;
  tema:=ETema.Text;
  autor:=EAutor.Text;
  if (edicao.Length<>0) and (tema.Length<>0) and (autor.Length<>0) then
  begin
    Jornal := LBJornal.Items.Objects[LBJornal.ItemIndex] as TJornal;
    Jornal.edicao := edicao;
    Jornal.tema := tema;
    Jornal.autor := autor;
    LBJornal.Items[LBJornal.ItemIndex]:=EEdicao.Text ;
  end
  else
  begin
    ShowMessage('Algum dos campos esta em branco.');
  end;

end;

procedure TForm1.btnCarregarClick(Sender: TObject);
var arq:TextFile;
var Jornal:TJornal;
begin
  AssignFile(arq, 'Jornal.txt');
  Reset(arq);

  Jornal:=TJornal.Create;

  while not Eof(arq) do
  begin
      Readln(arq, Jornal.edicao);
      Readln(arq, Jornal.tema);
      Readln(arq, Jornal.autor);

      LBJornal.Items.AddObject(Jornal.edicao, Jornal);
  end;
    CloseFile(arq);
end;

procedure TForm1.btnDeletarClick(Sender: TObject);
begin
  LBJornal.DeleteSelected;
end;

procedure TForm1.btnEnviarClick(Sender: TObject);
var jornal2 : TJornal;
var edicao, tema, autor: string;
begin
  edicao:=EEdicao.Text;
  tema:=ETema.Text;
  autor:=EAutor.Text;
  if (edicao.Length<>0) and (tema.Length<>0) and (autor.Length<>0) then
  begin
    jornal2 := TJornal.Create;
    jornal2.edicao := edicao;
    jornal2.tema := tema;
    jornal2.autor := autor;
    LBJornal.Items.AddObject(jornal2.edicao, jornal2);
  end
  else
  begin
    ShowMessage('Algum dos campos esta em branco.');
  end;

end;

procedure TForm1.btnSalvarClick(Sender: TObject);
var arq : TextFile;
var jornal:TJornal;
var i: integer;
begin
    if LBJornal.Items.Count<>0 then
    begin
      AssignFile(arq, 'Jornal.txt');
      Rewrite(arq);
      for i:=0 to Pred(LBJornal.Items.Count) do
      begin
        jornal := LBJornal.Items.Objects[i] as TJornal;
        writeln(arq, jornal.edicao);
        writeln(arq, jornal.tema);
        writeln(arq, jornal.autor);
      end;

      CloseFile(arq);
    end
    else
    begin
      ShowMessage('N�o h� o que salvar.');
    end;

end;
procedure TForm1.LBJornalClick(Sender: TObject);
begin
btnDeletar.Enabled:=true;
btnAtualizar.Enabled:=true;

end;

end.
