object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Tela de Inicio'
  ClientHeight = 290
  ClientWidth = 335
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Atualizar: TButton
    Left = 215
    Top = 8
    Width = 98
    Height = 39
    Caption = 'Atualizar'
    TabOrder = 0
    OnClick = AtualizarClick
  end
  object ListBox1: TListBox
    Left = 8
    Top = 8
    Width = 201
    Height = 252
    ItemHeight = 13
    TabOrder = 1
    OnDblClick = ListBox1DblClick
  end
  object Button1: TButton
    Left = 215
    Top = 219
    Width = 98
    Height = 41
    Caption = 'Cadastrar'
    TabOrder = 2
    OnClick = Button1Click
  end
  object NetHTTPClient1: TNetHTTPClient
    Asynchronous = False
    ConnectionTimeout = 60000
    ResponseTimeout = 60000
    AllowCookies = True
    HandleRedirects = True
    UserAgent = 'Embarcadero URI Client/1.0'
    Left = 248
    Top = 112
  end
end
