unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, jpeg, ExtCtrls;

type
  TJanela = class(TForm)
    esporte: TLabel;
    caixainserida: TListBox;
    TextoLabel: TLabel;
    Inserir: TButton;
    Apagar: TButton;
    Deletar: TButton;
    Editar: TButton;
    caixadetexto3: TEdit;
    Image1: TImage;
    carregar: TButton;
    caixadetexto2: TEdit;
    caixadetexto1: TEdit;
    Nome_Esporte: TLabel;
    Numero_Jogadores: TLabel;
    Tempo_Jogo: TLabel;
    procedure InserirClick(Sender: TObject);
    procedure ApagarClick(Sender: TObject);
    procedure EditarClick(Sender: TObject);
    procedure DeletarClick(Sender: TObject);
    procedure carregarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TEsportes = class (TObject)
  qualesporte : string;
  numerojogador : integer;
  tempo : string;
  end;

var
  Janela: TJanela;
  arquivo:TextFile;

implementation

{$R *.dfm}

procedure TJanela.InserirClick(Sender: TObject);
var
   Esporte: TEsportes;
begin
Esporte := TEsportes.Create;
Esporte.qualesporte:= caixadetexto1.Text;
Esporte.numerojogador:= strtoint(caixadetexto2.Text);
Esporte.tempo:= caixadetexto3.Text;
caixainserida.Items.AddObject(Esporte.qualesporte, Esporte);
if (caixadetexto1.Text = ' ') then
begin
writeln ('Digite seu esporte');
end;
end;

procedure TJanela.ApagarClick(Sender: TObject);
begin
caixainserida.DeleteSelected;
end;

procedure TJanela.EditarClick(Sender: TObject);
var
i:integer;
dados:string;
esporte : TEsportes;
jogador : TEsportes;
tempo : TEsportes;
begin
  For i := caixainserida.Items.Count - 1 downto 0 do
  begin
    if (caixainserida.selected[i]) then
    begin
      dados:= InputBox('Mudar o nome de ' +caixainserida.Items[i],'Novo Esporte : ','');
      if (Trim(dados) <> '') then begin
        caixainserida.Items[i] := dados;
        esporte := TEsportes(caixainserida.Items.Objects[i]);
        esporte.qualesporte := dados;
        caixainserida.Items.Objects[i] := esporte;
      end;
      dados:= InputBox('Mudar o nome de ' +caixainserida.Items[i],'Novo Numero De Jogadores : ','');
      if (Trim(dados) <> '') then begin
        caixainserida.Items[i] := dados;
        jogador := TEsportes(caixainserida.Items.Objects[i]);
        jogador.qualesporte := dados;
        caixainserida.Items.Objects[i] := jogador;
      end;
      dados:= InputBox('Mudar o nome de ' +caixainserida.Items[i],'Novo Tempo De Jogo : ','');
      if (Trim(dados) <> '') then begin
        caixainserida.Items[i] := dados;
        tempo := TEsportes(caixainserida.Items.Objects[i]);
        tempo.qualesporte := dados;
        caixainserida.Items.Objects[i] := tempo;
      end;

    end;
  end;
end;

procedure TJanela.DeletarClick(Sender: TObject);
var arq:TextFile;
var Esporte:TEsportes;
var I:integer;
begin
AssignFile(arq, 'Lista.txt');
Rewrite(arq);
for i:=0 to Pred(caixainserida.Items.Count) do
begin
Esporte:= caixainserida.Items.Objects[i] as TEsportes;
write(arq, Esporte.qualesporte+' | '+inttostr(Esporte.numerojogador)+' | '+Esporte.tempo);
end;
CloseFile(arq);
end;

procedure TJanela.carregarClick(Sender: TObject);
var linha : string;
begin
  if caixadetexto1.Text = '' then
  begin
    showmessage ('Digite o nome do arquivo para localizar!');
  end
  else
  begin
    caixainserida.Clear;
    AssignFile (arquivo, caixadetexto1.Text+'.txt');
    Reset (arquivo);
    if (IOResult <> 0) then
    begin
      caixainserida.Items.Add('arquivo N�ol Encontrado!');
    end
    else
    begin
      while (not eof (arquivo)) do
    begin

    readln (arquivo, linha);
    caixainserida.Items.Add(Linha);
  end;
  CloseFile(arquivo);
end;
end;

end;

end.
